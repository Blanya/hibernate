package org.example.services;

import org.example.dao.IDao;
import org.example.entities.FicheConsultation;

import java.util.List;

public class FicheConsultationService extends BaseService implements IDao<FicheConsultation> {
    @Override
    public boolean create(FicheConsultation o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(FicheConsultation o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(FicheConsultation o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public FicheConsultation findById(int id) {
        FicheConsultation ficheConsultation = session.get(FicheConsultation.class, id);
        return ficheConsultation;
        }

    @Override
    public List<FicheConsultation> findAll() {
       List<FicheConsultation> ficheConsultations = null;
       ficheConsultations = session.createQuery("FROM FicheConsultation").list();
       return ficheConsultations;
    }
}
