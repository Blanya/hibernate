package org.example.services;

import org.example.dao.IDao;
import org.example.entities.Consultation;

import java.util.Date;
import java.util.List;

public class ConsultationService extends BaseService implements IDao<Consultation> {

    @Override
    public boolean create(Consultation o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(Consultation o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(Consultation o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public Consultation findById(int id) {
        Consultation consultation = session.get(Consultation.class, id);
        return consultation;
    }

    @Override
    public List<Consultation> findAll() {
        List<Consultation> consultations = null;
        consultations = session.createQuery("FROM Consultation").list();
        return consultations;
    }

    //jointure => select consultation, join => fiche/dossiermedical/patient
    public List<Consultation> findAllByDateAndPatient(Date date, int id)
    {
        List<Consultation> consultations = null;
        return consultations;
    }
}
