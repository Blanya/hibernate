package org.example.services;

import org.example.dao.IDao;
import org.example.entities.FichePayement;

import java.util.List;

public class FichePayementService extends BaseService implements IDao<FichePayement> {
    @Override
    public boolean create(FichePayement o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(FichePayement o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(FichePayement o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public FichePayement findById(int id) {
        FichePayement fichePayement = session.get(FichePayement.class, id);
        return fichePayement;
    }

    @Override
    public List<FichePayement> findAll() {
        List<FichePayement> fichePayements = null;
        fichePayements = session.createQuery("FROM FichePayement").list();
        return fichePayements;
    }
}
