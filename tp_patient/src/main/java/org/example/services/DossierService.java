package org.example.services;

import org.example.dao.IDao;
import org.example.entities.DossierMedical;
import org.example.entities.Patient;

import java.util.List;

public class DossierService extends BaseService implements IDao<DossierMedical> {
    @Override
    public boolean create(DossierMedical o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(DossierMedical o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(DossierMedical o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public DossierMedical findById(int id) {
        //Si non toruvé return null
        DossierMedical dossierMedical = session.get(DossierMedical.class, id);
        return dossierMedical;
    }

    @Override
    public List<DossierMedical> findAll() {
        List<DossierMedical> dossierMedicals = null;
        dossierMedicals = session.createQuery("FROM DossierMedical").list();
        return dossierMedicals;
    }

    public DossierMedical findByPatient(Patient patient)
    {
        DossierMedical dossierMedical = null;
        dossierMedical = patient.getDossierMedical();
        return dossierMedical;
    }

}
