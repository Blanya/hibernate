package org.example.services;

import org.example.dao.IDao;
import org.example.entities.Patient;

import java.util.List;

public class PatientService extends BaseService implements IDao<Patient> {
    @Override
    public boolean create(Patient o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(Patient o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(Patient o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public Patient findById(int id) {
        //Si non toruvé return null
        Patient patient = session.get(Patient.class, id);
        return patient;
    }

    @Override
    public List<Patient> findAll() {
        List<Patient> patients = null;
        patients = session.createQuery("FROM Patient").list();
        return patients;
    }
}
