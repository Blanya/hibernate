package org.example.services;

import org.example.dao.IDao;
import org.example.entities.Prescription;

import java.util.List;

public class PrescriptionService extends BaseService implements IDao<Prescription> {

    @Override
    public boolean create(Prescription o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean update(Prescription o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public boolean delete(Prescription o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }

    @Override
    public Prescription findById(int id) {
        Prescription prescription = session.get(Prescription.class, id);
        return prescription;
    }

    @Override
    public List<Prescription> findAll() {
        List<Prescription> prescriptions = null;
        prescriptions = session.createQuery("FROM Prescription").list();
        return prescriptions;
    }
}
