package org.example;

import org.example.entities.*;
import org.example.services.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.example.services.BaseService;

public class Ihm {

    private static ConsultationService consultationService = new ConsultationService();
    private static DossierService dossierService = new DossierService();
    private static FicheConsultationService ficheConsultationService = new FicheConsultationService();
    private static FichePayementService fichePayementService = new FichePayementService();
    private static OperationAnalyseService operationAnalyseService = new OperationAnalyseService();
    private static PatientService patientService = new PatientService();
    private static PrescriptionService prescriptionService = new PrescriptionService();
    private static Scanner sc = new Scanner(System.in);

    public static void printMenu()
    {
        String choice;

        do{
            String[] propositions = {"--- Menu Hopital ---", "1. Ajouter un patient", "2. Créer un dossier médical",
                "3. Créer une consultation", "4. Afficher le dossier médical d'un patient", "5. Rechercher une consultation par date et par patient", "0. Exit"};

            System.out.println();
            for (String p : propositions) {
                System.out.println(p);
            }

            choice = sc.next();

            switch (choice) {
                case "1":
                    createPatient();
                    break;
                case "2":
                    createDossier();
                    break;
                case "3":
                    createConsultation();
                    break;
                case "4":
                    showDossier();
                    break;
                case "5":
                    showConsultation();
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Commande invalide, veuillez réessayer");
                break;
        }
    }while (!choice.equals("0"));
        //close session
        BaseService.session.close();
}

public static Patient createPatient(){
    System.out.println("Ajout d'un patient");
    try {
        System.out.println("Numéro de sécurité sociale : ");
        String num = sc.next();
        System.out.println("Prenom : ");
        String prenom = sc.next();
        System.out.println("Nom : ");
        String nom = sc.next();
        System.out.println("Date de naissance (JJ/MM/AAAA) : ");
        String dateString = sc.next();

        char genre;
        do {
            System.out.println("Sexe : (M, F, A)");
            String sexe = sc.next();
            char[] genres = sexe.toCharArray();
            genre = genres[0];
        }while (genre != 'M' && genre != 'F' && genre != 'A');


        System.out.println("Adresse : ");
        sc.nextLine();
        String adresse = sc.nextLine();
        System.out.println("Numéro de téléphone");
        String tel = sc.next();

        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);

        Patient patient = new Patient(num, prenom, nom, date, genre, adresse, tel);
        boolean result = patientService.create(patient);

        if (result)
        {
            System.out.println("Le patient a bien été ajouté");
            return patient;
        }
        else
        {
            System.out.println("Erreur lors de l'ajout du patient");
            return null;
        }
    } catch (InputMismatchException ex) {
        System.out.println("Données incorrectes, veuillez réessayer.");
        createPatient();
    } catch (ParseException e) {
        System.out.println("Format de la date incorrect");
        createPatient();
    }
    return null;
}

public static void createDossier(){
    System.out.println("Création d'un dossier médical");
    try {
        System.out.println("Date de création (JJ/MM/AAAA) : ");
        String dateString = sc.next();
        System.out.println("Code d'accès du patient: ");
        String code = sc.next();

        Date dateDossier = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);

        DossierMedical dossierMedical = new DossierMedical(dateDossier, code);

        String choice = "0";
        do {
            System.out.println("Souhaitez-vous l'associer à un patient existant ou créer un nouveau patient ? 1- Associer, 2- Créer");
            choice = sc.next();
        }while(!Objects.equals(choice, "1") && !Objects.equals(choice, "2"));

        //Patient null => récupérer si patient existant ou création de patient
        Patient patient = null;

        //Associer à un patient existant
        if(choice.equals("1"))
        {
            System.out.println("A quel patient souhaitez-vous l'associer ? Veuillez entrer l'ID svp");
            int id = sc.nextInt();

            patient = patientService.findById(id);
        }

        //Créer un patient
        else if (choice.equals("2")) {
            patient = createPatient();
        }

        //Vérifier si patient bien créé ou existe
        if(patient != null)
        {
            boolean createDossier =dossierService.create(dossierMedical);
            dossierMedical.setPatient(patient);
            patientService.update(patient);

            if(createDossier)
                System.out.println("Le dossier a bien été créé");
            else
            {
                System.out.println("Erreur lors de la création du dossier, veuillez-réessayer");
            }
        }
        else
        {
            System.out.println("Le patient sélectionné n'est pas dans nos données, veuillez-réessayer svp");
            createDossier();
        }

    } catch (InputMismatchException ex) {
        System.out.println("Données incorrectes, veuillez réessayer.");
        createDossier();
    } catch (ParseException e) {
        System.out.println("Format de la date incorrect");
        createDossier();
    }
}

public static void createConsultation(){
    System.out.println("Création d'une consultation");

    // L'associer a un patient => associer les fiches à celui-ci
    try {
        int id = 0;
        try
        {
            System.out.println("Quel est l'id du patient ?");
            id = sc.nextInt();
        }catch (InputMismatchException e)
        {
            System.out.println("Saisie incorrecte, veuillez réessayer");
            createConsultation();
        }

        Patient patient = patientService.findById(id);

        if(patient != null)
        {
            System.out.println("Date de la consultation (JJ/MM/AAAA) : ");
            String dateString = sc.next();
            System.out.println("Heure (HH:MM) : ");
            String heure = sc.next();
            System.out.println("Lieu : ");
            sc.nextLine();
            String lieu = sc.nextLine();

            Date date = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(dateString + " " + heure);

            //Etat A pour annulé, C pour confirmer => auto = confirmer
            Consultation consultation = new Consultation(date,heure, lieu);

            //Juste enregister consultation pour plus tard ou ajouter toutes les données si consultation déjà effectuée
            String choice = "0";
            do {
                System.out.println("Souhaitez-vous ajouter le compte rendu ainsi que les prescriptions déjà effectuées? ? 1- Oui, 2- Non, la consultation s'effectuera plus tard (peut-être annulée)");
                choice = sc.next();
            }while(!Objects.equals(choice, "1") && !Objects.equals(choice, "2"));

            //Associer à une fiche + ope etc;
            if(choice.equals("1"))
            {
                //pour vérifier date compte rendu >= date consultation
                Date dateC;
                do {
                    System.out.println("Date du compte-rendu (JJ/MM/AAAA) : ");
                    String dateS = sc.next();
                    dateC = new SimpleDateFormat("dd/MM/yyyy").parse(dateS);
                }while (dateC.before(date));

                System.out.println("Agent créateur de la fiche");
                sc.nextLine();
                String agent = sc.nextLine();

                System.out.println("Adresse du créateur de la fiche");
                String adresse = sc.nextLine();

                System.out.println("Quel est le compte-rendu de la consultation? ");
                String compteRendu = sc.nextLine();

                //créer fiche
                FicheConsultation ficheConsultation = new FicheConsultation(dateC, agent, adresse, compteRendu);

                //ajout d'une ou plusieurs prescriptions => 1 min
                String p = "1";
                //list des prescriptions
                List<Prescription> prescriptions = new ArrayList<>();
                do {
                    if(p.equals("1"))
                    {
                        System.out.println("Désignation de la prescription: ");
                        sc.nextLine();
                        String designation = sc.nextLine();
                        System.out.println("Période : ");
                        String periode = sc.nextLine();
                        System.out.println("Indication (Si aucune indication complémentaire inscrire 0 ");
                        String indication = sc.nextLine();

                        Prescription prescription;
                        if(indication.charAt(0) == '0')
                        {
                            prescription = new Prescription(designation, periode);
                        }

                        else
                        {
                            prescription = new Prescription(designation, periode, indication);
                        }

                        prescriptions.add(prescription);
                    }

                    System.out.println("Souhaitez-vous ajoutez une prescription? 1. oui, 2. non");
                    p = sc.next();

                }while (!Objects.equals(p, "2"));

                //ajouter les prescriptions a la fiche consultation
                ficheConsultation.setPrescriptions(prescriptions);

                // ajout possible d'une ou plusieurs opérations
                String c = "0";
                do {
                    System.out.println("Souhaitez-vous ajouter une operation/ analyse effectuée ? 1. oui, 2. non");
                    c = sc.next();
                }while (!c.equals("1") && !c.equals("2"));

                //Créer op => recuperer liste depuis une autre fonction
                if(c.equals("1"))
                {
                    List<OperationAnalyse> operationAnalyses = addOp();
                    ficheConsultation.setAnalyses(operationAnalyses);
                }

                //Créer la fiche de paiement
                System.out.println("Création de la fiche de paiement");
                System.out.println("Date de l'exigibilité du paiement (JJ/MM/AAAA) : ");
                String dateS = sc.next();
                Date dateExi = new SimpleDateFormat("dd/MM/yyyy").parse(dateS);

                System.out.println("Quel est le montant à payer? ");
                double montant = sc.nextDouble();

                //adresse + agent + date récupérés => mm que compte rendu

                String paiement = "1";
                do {
                    System.out.println("Un paiement a déjà été donné ? 1. oui, 2. non");
                    paiement = sc.next();
                }while (!paiement.equals("1") && !paiement.equals("2"));

                FichePayement fichePayement = null;
                if(paiement.equals("1"))
                {
                    double price = 0;
                    do {
                        System.out.println("De combien est le montant effectué ?");
                        price = sc.nextDouble();
                    }while (price > montant);

                    System.out.println("A quelle date a été perçu le paiement ? ");
                    String dateStringPaiement = sc.next();
                    Date datePaiement = new SimpleDateFormat("dd/MM/yyyy").parse(dateStringPaiement);


                    if(price < montant)
                    {
                       fichePayement = new FichePayement(dateC, agent, adresse, dateExi, datePaiement, montant, price, false);
                    }
                    else
                    {
                        fichePayement = new FichePayement(dateC, agent, adresse, dateExi, datePaiement, montant, price, true);
                    }
                } else if (paiement.equals("2")) {
                    fichePayement = new FichePayement(dateC, agent, adresse, dateExi, montant);
                }

                //Recupérer dossier médical
                DossierMedical dossierMedical = dossierService.findByPatient(patient);

                consultationService.create(consultation);

                ficheConsultation.setOrigineFiche(consultation);

                dossierMedical.addFiches(fichePayement);
                dossierMedical.addFiches(ficheConsultation);

                //Creation fiche payement
                boolean ficheCheck = fichePayementService.create(fichePayement);

                if(ficheCheck)
                    System.out.println("Fiche de payement créée");

                //Creation fiche consultation
                boolean createConsult = ficheConsultationService.create(ficheConsultation);

                if (createConsult)
                    System.out.println("Consultation créée !");

                boolean updateDossier = dossierService.update(dossierMedical);

                if (updateDossier)
                    System.out.println("Dossier médical mis à jour");
                else
                    System.out.println("Une erreur est survenue");
            }

            //Créer la consultation seule
            else if (choice.equals("2")) {
                boolean consult = consultationService.create(consultation);

                if(consult)
                    System.out.println("La consultation est bien ajoutée");
                else
                {
                    System.out.println("Erreur lors de la création de la consultation");
                    createConsultation();
                }
            }
        }
        else
        {
            System.out.println("Patient introuvable, veuillez réessayer.");
        }

    } catch (InputMismatchException ex) {
        System.out.println("Données incorrectes, veuillez réessayer.");
        createPatient();
    } catch (ParseException e) {
        System.out.println("Format de la date incorrect");
        createPatient();
    }
}

public static List<OperationAnalyse> addOp(){
    try{
        String choice = "1";
        List<OperationAnalyse> operationAnalyses = new ArrayList<>();
        do {
            System.out.println("Description : ");
            sc.nextLine();
            String designation = sc.nextLine();
            System.out.println("Date Opération (Format JJ/MM/AAAA) :");
            String dateS = sc.next();
            System.out.println("Heure (Format HH:MM) :");
            String heureS = sc.next();
            System.out.println("Résultat de l'opération : ");
            sc.nextLine();
            String resultat = sc.nextLine();

            Date date = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(dateS + " " + heureS);

            OperationAnalyse operationAnalyse = new OperationAnalyse(designation, date, resultat);
            operationAnalyses.add(operationAnalyse);

            System.out.println("Ajouter une autre opération? 1. oui, n'importe quel caractère pour quitter");
            choice = sc.next();
        }while (choice.equals("1"));
        return operationAnalyses;
    }catch (ParseException e)
    {
        System.out.println("Format de la date incorrect, veuillez-recommencez svp");
        addOp();
    }
    return null;
}

public static void showDossier(){
    System.out.println("Afficher le dossier d'un patient");
    try{
        System.out.println("Donnez moi l'id du patient");
        int id = sc.nextInt();

        Patient patient = patientService.findById(id);

        if(patient != null)
        {
            DossierMedical dossierMedical = dossierService.findByPatient(patient);
            System.out.println(dossierMedical);
        }
        else
            System.out.println("Patient introuvable, veuillez-réessayer");
    }catch (Exception e)
    {
        System.out.println(e.getMessage());
    }
}

public static void showConsultation(){

}

}
