package org.example.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "consultation")
public class Consultation {

    @Id
    @Column(name = "num_cons")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int numeroConsultation;

    @Column(name= "date_cons")
    private Date dateConsultation;

    @Column(name = "heure_cons")
    private String heureConsultation;

    private String lieu;

    @Column(name = "etat_cons")
    private char etatConsultation;

    public Consultation() {
    }

    public Consultation(Date dateConsultation, String heureConsultation, String lieu) {
        this.dateConsultation = dateConsultation;
        this.heureConsultation = heureConsultation;
        this.lieu = lieu;
        //AUTO etat confirmé
        this.etatConsultation = 'C';
    }

    public Consultation(Date dateConsultation, String heureConsultation, String lieu, char etatConsultation) {
        this.dateConsultation = dateConsultation;
        this.heureConsultation = heureConsultation;
        this.lieu = lieu;
        this.etatConsultation = etatConsultation;
    }

    public int getNumeroConsultation() {
        return numeroConsultation;
    }

    public void setNumeroConsultation(int numeroConsultation) {
        this.numeroConsultation = numeroConsultation;
    }

    public Date getDateConsultation() {
        return dateConsultation;
    }

    public void setDateConsultation(Date dateConsultation) {
        this.dateConsultation = dateConsultation;
    }

    public String getHeureConsultation() {
        return heureConsultation;
    }

    public void setHeureConsultation(String heureConsultation) {
        this.heureConsultation = heureConsultation;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public char getEtatConsultation() {
        return etatConsultation;
    }

    public void setEtatConsultation(char etatConsultation) {
        this.etatConsultation = etatConsultation;
    }

    //demande consult => créer consultation


    //annule consult => changer l'état de la consultation
    public void annulerConsultation()
    {
        this.etatConsultation = 'A';
    }

    @Override
    public String toString() {
        return "Consultation{" +
                "numeroConsultation=" + numeroConsultation +
                ", dateConsultation=" + dateConsultation +
                ", heureConsultation='" + heureConsultation + '\'' +
                ", lieu='" + lieu + '\'' +
                ", etatConsultation=" + etatConsultation +
                '}';
    }
}
