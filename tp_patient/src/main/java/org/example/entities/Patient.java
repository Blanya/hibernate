package org.example.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "patient")
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="pa_nss")
    private String numeroSecu;

    private String prenom;

    private String nom;

    @Column(name = "date_de_naissance")
    @Temporal(TemporalType.DATE)
    private Date dataNaissance;

    private char sexe;

    private String adresse;

    private String tel;

    @OneToOne
    @JoinColumn(name = "num_do")
    private DossierMedical dossier;

    public Patient() {
    }

    public Patient(String numeroSecu, String prenom, String nom, Date dataNaissance, char sexe, String adresse, String tel) {
        this.numeroSecu = numeroSecu;
        this.prenom = prenom;
        this.nom = nom;
        this.dataNaissance = dataNaissance;
        this.sexe = sexe;
        this.adresse = adresse;
        this.tel = tel;
    }

    public String getNumeroSecu() {
        return numeroSecu;
    }

    public void setNumeroSecu(String numeroSecu) {
        this.numeroSecu = numeroSecu;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDataNaissance() {
        return dataNaissance;
    }

    public void setDataNaissance(Date dataNaissance) {
        this.dataNaissance = dataNaissance;
    }

    public char getSexe() {
        return sexe;
    }

    public void setSexe(char sexe) {
        this.sexe = sexe;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public DossierMedical getDossierMedical() {
        return dossier;
    }

    public void setDossierMedical(DossierMedical dossierMedical) {
        this.dossier = dossierMedical;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "numeroSecu='" + numeroSecu + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", dataNaissance=" + dataNaissance +
                ", sexe=" + sexe +
                ", adresse='" + adresse + '\'' +
                ", tel='" + tel + '\'' +
                ", dossierMedical=" + dossier+
                '}';
    }
}
