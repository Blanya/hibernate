package org.example.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "dossier_medical")
public class DossierMedical {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "num_do")
    private int numero;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_creation")
    private Date dateCreation;

    @Column(name= "code_acces_patient")
    private String codeAccesPatient;

    @OneToOne(mappedBy = "dossier")
    private Patient patient;

    @OneToMany(mappedBy = "dossier")
    private List<FicheDeSoin> fiches = new ArrayList<>();

    public DossierMedical() {
    }

    public DossierMedical(Date dateCreation, String codeAccesPatient) {
        this.dateCreation = dateCreation;
        this.codeAccesPatient = codeAccesPatient;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getCodeAccesPatient() {
        return codeAccesPatient;
    }

    public void setCodeAccesPatient(String codeAccesPatient) {
        this.codeAccesPatient = codeAccesPatient;
    }

    @OneToOne(mappedBy = "dossier")
    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
        //update dossier medical in patient
        patient.setDossierMedical(this);
    }

    public List<FicheDeSoin> getFiches() {
        return fiches;
    }

    public void setFiches(List<FicheDeSoin> fiches) {
        this.fiches = fiches;
    }

    public void addFiches(FicheDeSoin fiche)
    {
        this.fiches.add(fiche);
        fiche.setDossier(this);
    }

    @Override
    public String toString() {
        return "DossierMedical{" +
                "numero=" + numero +
                ", dateCreation=" + dateCreation +
                ", codeAccesPatient='" + codeAccesPatient +
                ", fiches=" + fiches +
                '}';
    }
}
