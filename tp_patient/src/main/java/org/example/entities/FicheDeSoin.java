package org.example.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "fiche_de_soin")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class FicheDeSoin implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int numeroFiche;

    private Date dateCreation;

    private String agentCreateur;

    private String adresseCreateur;

    @ManyToOne
    @JoinColumn(name = "num_do")
    private DossierMedical dossier;

    //Recuperation des fiches deja payés
//    private List<FicheDeSoin> fichePayes;

    public FicheDeSoin() {
    }

    public FicheDeSoin(Date dateCreation, String agentCreateur, String adresseCreateur) {
        this.dateCreation = dateCreation;
        this.agentCreateur = agentCreateur;
        this.adresseCreateur = adresseCreateur;
    }

    public int getNumeroFiche() {
        return numeroFiche;
    }

    public void setNumeroFiche(int numeroFiche) {
        this.numeroFiche = numeroFiche;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getAgentCreateur() {
        return agentCreateur;
    }

    public void setAgentCreateur(String agentCreateur) {
        this.agentCreateur = agentCreateur;
    }

    public String getAdresseCreateur() {
        return adresseCreateur;
    }

    public void setAdresseCreateur(String adresseCreateur) {
        this.adresseCreateur = adresseCreateur;
    }

    public DossierMedical getDossier() {
        return dossier;
    }

    public void setDossier(DossierMedical dossier) {
        this.dossier = dossier;
    }

    @Override
    public String toString() {
        return "FicheDeSoin{" +
                "numeroFiche=" + numeroFiche +
                ", dateCreation=" + dateCreation +
                ", agentCreateur='" + agentCreateur + '\'' +
                ", adresseCreateur='" + adresseCreateur + '\'' +
                '}';
    }
}
