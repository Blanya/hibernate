package org.example.service;

import org.example.dao.IDao;
import org.example.entities.Commentaire;
import org.example.entities.Image;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

public class CommentaireService implements IDao<Commentaire> {

    static StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    static SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    public static Session session = sessionFactory.openSession();
    @Override
    public boolean create(Commentaire o) {
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Commentaire o) {
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(Commentaire o) {
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public Commentaire findById(int id) {
        Commentaire commentaire = null;
        session.beginTransaction();
        commentaire = (Commentaire) session.get(Commentaire.class, id);
        session.getTransaction().commit();
        return commentaire;
    }

    @Override
    public List<Commentaire> findAll() {
        session.beginTransaction();
        List<Commentaire> commentaires = null;
        commentaires = session.createQuery("FROM Commentaire").list();
        session.getTransaction().commit();
        return commentaires;
    }
}
