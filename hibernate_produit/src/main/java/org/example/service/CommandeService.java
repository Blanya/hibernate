package org.example.service;

import org.example.dao.IDao;
import org.example.entities.Commande;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.Date;
import java.util.List;

public class CommandeService implements IDao<Commande> {

    static StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    static SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    public static Session session = sessionFactory.openSession();

    @Override
    public boolean create(Commande o) {
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Commande o) {
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(Commande o) {
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public Commande findById(int id) {
        Commande commande = null;
        session.beginTransaction();
        commande = (Commande) session.get(Commande.class, id);
        session.getTransaction().commit();
        return commande;
    }

    @Override
    public List<Commande> findAll() {
        List<Commande> commandes = null;
        commandes = session.createQuery("FROM Commande").list();
        return commandes;
    }

    public List<Commande> findOrderByDate(Date date) {
        List<Commande> commandes = null;
        commandes = session.createQuery("FROM Commande WHERE dateAchat = :date").setParameter("date", date).list();
        return commandes;
    }
}
