package org.example.service;

import org.example.entities.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.example.service.ProduitService.session;

public class Menu {

    static ProduitService daoProduit = new ProduitService();
    static ImageService daoImage = new ImageService();
    static CommentaireService daoCommentaire = new CommentaireService();
    static CommandeService daoCommande = new CommandeService();
    static AdresseService daoAdresse = new AdresseService();

    static Scanner sc = new Scanner(System.in);

    public static void printMenu() {

        String choice = "0";

        do
        {
            String[] propositions = {"--- Menu Produits ---", "1. Créer un produit", "2. Trouver un produit par id",
                    "3. Supprimer un produit par id", "4. Modifier un produit par id", "5. Afficher la liste des produits",
                    "6. Afficher les produits dont le prix est > à un nombre donné", "7. Afficher les produits compris entre deux dates", "8. Afficher les produits dont le stock est inférieur à la saisie", "9. Afficher la valeur du stock des produits de la marque saisie", "10. Afficher le prix moyen des produits ", "11. Afficher la liste des produits d'une ou plusieurs marques données", "12. Supprimer les produits d'une marque", "13. Ajouter une image à un produit existant", "14. Ajouter un commentaire à un produit", "15. Afficher les produits les mieux notés", "16. Effectuer une commande", "17. Afficher la totalité des commandes", "18. Afficher les commandes selon une date", "0. Exit"};

            System.out.println();
            for (String p : propositions) {
                System.out.println(p);
            }

            choice = sc.next();

            switch (choice) {
                case "1":
                    createProduct();
                    break;
                case "2":
                    findProduct();
                    break;
                case "3":
                    deleteProduct();
                    break;
                case "4":
                    updateProduct();
                    break;
                case "5":
                    showAllProducts();
                    break;
                case "6":
                    findProductWithPrice();
                    break;
                case "7":
                    findProductBetween2Dates();
                    break;
                case "8":
                    findProductsWithStock();
                    break;
                case "9":
                    findValueProductsByBrand();
                    break;
                case "10":
                    avgProducts();
                    break;
                case "11":
                    findProductsByBrand();
                    break;
                case "12":
                    deleteProductsByBrand();
                    break;
                case "13":
                    addImageAtProduct();
                    break;
                case "14":
                    addCommentAtProduct();
                    break;
                case "15":
                    getBestProducts();
                    break;
                case "16":
                    haveOrder();
                    break;
                case "17":
                    findAllOrders();
                    break;
                case "18":
                    findOrdersByDate();
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Commande invalide, veuillez réessayer");
                    break;
            }
        }while (!choice.equals("0"));

        session.close();
    }

    public static void createProduct() {
        System.out.println("Création d'un produit");
        try {
            System.out.println("Marque produit : ");
            sc.nextLine();
            String marque = sc.nextLine();
            System.out.println("Ref du produit : ");
            String reference = sc.next();
            System.out.println("Date d'achat du produit (JJ/MM/AAAA) : ");
            String dateString = sc.next();
            System.out.println("Prix du produit : ");
            double prix = sc.nextDouble();
            System.out.println("Stock du produit : ");
            int stock = sc.nextInt();

            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);

            Produit produit = new Produit(marque, reference, date, prix, stock);
            boolean result = daoProduit.create(produit);

            if (result)
                System.out.println("Le produit a bien été créé");
            else
                System.out.println("Erreur lors de la création du produit");

        } catch (InputMismatchException ex) {
            System.out.println("Données incorrectes, veuillez réessayer.");
            createProduct();
        } catch (ParseException e) {
            System.out.println("Format de la date incorrect");
            createProduct();
        }
    }

    public static void findProduct() {
        System.out.println("Trouver un produit par son id");
        try {
            System.out.println("Id du produit : ");
            int id = sc.nextInt();

            Produit produit = daoProduit.findById(id);
            if (produit != null) {
                System.out.println("Voici le produit trouvé");
                System.out.println(produit);
            } else
                System.out.println("Le produit n'existe pas");

        } catch (InputMismatchException e) {
            System.out.println("Veuillez entrez l'id du produit");
            findProduct();
        }
    }

    public static void deleteProduct() {
        System.out.println("Suppression d'un produit par son id");
        try {
            System.out.println("Id du produit à supprimer : ");
            int id = sc.nextInt();

            Produit produit = daoProduit.findById(id);

            if (produit != null) {
                boolean result = daoProduit.delete(produit);
                if (result)
                    System.out.println("Le produit a bien été supprimé");
                else
                    System.out.println("Erreur de la suppression du produit");
            } else
                System.out.println("Le produit n'existe pas");

        } catch (InputMismatchException e) {
            System.out.println("Veuillez entrez l'id du produit à supprimer");
            deleteProduct();
        }
    }

    public static void updateProduct() {
        System.out.println("Modifier un produit par id");
        System.out.println("Donnez-moi le produit à modifier et les élèments à modifier");
        System.out.println("Id du produit");

        try {
            int id = sc.nextInt();
            Produit p = daoProduit.findById(id);

            // p == null if id doesn't exist product same
            if (p == null) {
                System.out.println("Le produit sélectionné n'existe pas, veuillez entrer un numéro de produit valide");
                updateProduct();
            } else {
                //get product values and new values
                System.out.printf("Marque[%s] :", p.getMarque());
                sc.nextLine();
                String marque = sc.nextLine();

                System.out.printf("Prix[%f] :", p.getPrix());
                double price = sc.nextDouble();

                System.out.printf("Stock[%d] :", p.getStock());
                int quantity = sc.nextInt();

                System.out.printf("Ref[%s] :", p.getReference());
                String ref = sc.next();

                System.out.printf("Date d'achat du produit (JJ/MM/AAAA)[%s] : ", p.getDateAchat());
                String dateString = sc.next();

                Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);

                p.setMarque(marque);
                p.setPrix(price);
                p.setStock(quantity);
                p.setReference(ref);
                p.setDateAchat(date);

                boolean result = daoProduit.update(p);

                if (result)
                    System.out.println("Le produit a bien été modifié");
                else
                    System.out.println("Erreur lors de la modification");

            }
        } catch (InputMismatchException e) {
            System.out.println("Les données insérées sont incorrectes, veuillez réessayer");
            updateProduct();
        } catch (ParseException e) {
            System.out.println("Format de la date incorrect, veuillez réessayer");
            updateProduct();
        }
    }

    public static void showAllProducts()
    {
        System.out.println("Afficher la liste des produits");
        List<Produit> produitList = daoProduit.findAll();
        if (produitList != null)
            produitList.forEach(System.out::println);
        else
            System.out.println("Pas encore de produit enregistré");
    }
    public static void findProductWithPrice() {
        System.out.println("Afficher les produits dont le prix est > à un nombre donné");
        try {
            System.out.println("Veuillez entrez un prix minimum :");
            double price = sc.nextDouble();
            List<Produit> produitsList = daoProduit.findAllProductsPriceUpThanNumber(price);
            if (produitsList != null)
                produitsList.forEach(System.out::println);
            else
                System.out.println("Aucun produit ne correspond à votre demande");
        } catch (InputMismatchException e) {
            System.out.println("Veuillez entrez un nombre");
            findProductWithPrice();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void findProductBetween2Dates() {
        System.out.println("Afficher les produits compris entre deux dates");
        try {
            System.out.println("Donnez-moi une date au format JJ/MM/AAAA");
            String dateS = sc.next();
            System.out.println("Donnez-moi une autre date au format JJ/MM/AAAA, plus récente que la première");
            String date2S = sc.next();

            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateS);
            Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(date2S);

            List<Produit> products = daoProduit.findAllProductsBetweenTwoDates(date, date2);
            System.out.printf("Voici les produits compris entre le %s et le %s", dateS, date2S);
            products.forEach(System.out::println);

        } catch (ParseException e) {
            System.out.println("Erreur de format de dates");
            findProductBetween2Dates();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void findProductsWithStock() {
        System.out.println("Afficher les produits dont le stock est inférieur à la saisie");
        try {
            System.out.println("Donnez-moi une limite de stock");
            int stock = sc.nextInt();

            List resultList = daoProduit.findProductsWithStock(stock);

            List<Produit> produits = new ArrayList<>();

//            for (Object o: resultList) {
//                Object[] res = (Object[]) o;
//                Produit pr = new Produit();
//                pr.setId((int) res[0]);
//                pr.setReference((String) res[1]);
//
//                produits.add(pr);
//            }
//
//            System.out.printf("Voici les produits dont le stock est inférieur à %d", stock);
//            produits.forEach(System.out::println);

            System.out.printf("Voici les produits(ref + id) dont le stock est inférieur à %d", stock);
            System.out.println();

            for (Object o : resultList) {
                Object[] res = (Object[]) o;
                System.out.println("Id: " + res[0] + ", Reference: " + res[1]);
            }

        } catch (InputMismatchException e) {
            System.out.println("Erreur de format, veuillez entrer un nombre entier");
            findProductsWithStock();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        session.close();
    }

    public static void findValueProductsByBrand()
    {
        System.out.println("Valeur des produits d'une marque donnée");
        try {
            System.out.println("De quelle marque voulez-vous connaître la valeur?");
            sc.nextLine();
            String marque = sc.nextLine();

            double value = daoProduit.showValueByBrand(marque);

            if(value > -1)
            {
                System.out.printf("La valeur des produits %s s'élève à %f", marque, value);
            }
            else
                System.out.println("Nous ne trouvons pas de produits de la marque");

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void avgProducts(){
        System.out.println("Prix moyen des produits");
        double price = daoProduit.showAvg();
        if(price>-1)
            System.out.println("Le prix moyen des produits est de " + price);
        else
            System.out.println("Pas de produits enregistrés");
    }

    public static void findProductsByBrand()
    {
        System.out.println("Afficher les produits d'une marque");
        try {
            List<String> marques = new ArrayList<>();
            int choice = 0;
            do {
                System.out.println("De quelle marque voulez-vous connaître les produits?");
                sc.nextLine();
                String marque = sc.nextLine();
                marques.add(marque);
                System.out.println("Souhaitez-vous afficher les produits d'une autre marque également? 0. oui, 1. Non");
                try{
                    choice = sc.nextInt();
                }catch (InputMismatchException e)
                {
                    System.out.println("Saisie incorrecte");
                }
            }while (choice == 0);

            String[] marquesArray = new String[marques.size()];
            marquesArray = marques.toArray(marquesArray);

            List<Produit> produits = daoProduit.showProductsByBrand(marquesArray);

            if(produits != null)
            {
                System.out.printf("Voici les produits des marques données");
                produits.forEach(System.out::println);
            }
            else
                System.out.println("Nous ne trouvons pas de produits correspondant aux marques");

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void deleteProductsByBrand()
    {
        System.out.println("Supprimer les produits d'une marque");
        try {
            System.out.println("De quelle marque voulez-vous supprimer les produits?");
            sc.nextLine();
            String marque = sc.nextLine();

            int result = daoProduit.deleteProductsByBrand(marque);
            if(result>0)
                System.out.printf("Les produits de la marque %s ont été supprimé", marque);
            else
                System.out.println("Nous n'avons pas de produits de cette marque enregistrés");
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void addImageAtProduct()
    {
        System.out.println("Ajouter une image à un produit existant");
        try {
            System.out.println("A quel produit souhaitez-vous ajouter une image ? Donnez moi l'id");

            int id = sc.nextInt();

            Produit produit = daoProduit.findById(id);

            if(produit != null)
            {
                System.out.println("Quelle est l'url de l'image à ajouter?");
                sc.nextLine();
                String url = sc.nextLine();

                Image image = new Image(url);
                produit.addImage(image);
                daoImage.create(image);
                daoProduit.update(produit);

            }
            else
                System.out.println("Produit introuvable, veuillez le créer svp");

        }catch (InputMismatchException e)
        {
            System.out.println("Veuillez entrer l'id du produit");
            addImageAtProduct();
        }
    }

    public static void addCommentAtProduct()
    {
        System.out.println("Ajouter un commentaire à un produit existant");
        try {
            System.out.println("A quel produit souhaitez-vous ajouter un commentaire ? Donnez moi l'id du produit");

            int id = sc.nextInt();

            Produit produit = daoProduit.findById(id);

            if(produit != null)
            {
                System.out.println("Quel est le contenu du commentaire à ajouter?");
                sc.nextLine();
                String description = sc.nextLine();

                //Si date non par défaut
                System.out.println("Quelle est la date ? format(JJ/MM/AAAA)");
                String date = sc.next();

                double note = -1;
                do {
                    System.out.println("Quelle est la note du produit? Entre 0 et 5");
                    note = sc.nextDouble();
                } while (note < 0 || note > 5);


                //Formatage date
                Date dateCommentaire = new SimpleDateFormat("dd/MM/yyyy").parse(date);

                Commentaire commentaire = new Commentaire(description, dateCommentaire, note);
                produit.addCommentaire(commentaire);
                daoCommentaire.create(commentaire);
                daoProduit.update(produit);

            }
            else
                System.out.println("Produit introuvable, veuillez le créer svp");

        }catch (InputMismatchException e)
        {
            System.out.println("Erreur lors de l'entrée des données");
            addCommentAtProduct();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static void getBestProducts()
    {
        double note = 4;
        try {

            List<Produit> produits = daoProduit.getBestProduct(note);
            produits.forEach(System.out::println);

        }catch (Exception e)
        {
            System.out.printf("Aucun produit n'a pour le moment de note moyenne supérieur a %f", note);
            System.out.println();
        }
    }

    public static void haveOrder()
    {
        System.out.println("Passer une commande");
        try {
            int choice = 1;
            //liste des produits de la commande
            List<Produit> produits = new ArrayList<>();
            do {
                System.out.println("Quel produit souhaitez-vous commander? Donnez moi l'id");

                int id = sc.nextInt();

                Produit produit = daoProduit.findById(id);

                if(produit != null && produit.getStock() > 0)
                {
                    produits.add(produit);
                    //retirer un du stock du produit
                    produit.setStock(produit.getStock() - 1 );
                    daoProduit.update(produit);
                }
                else
                    System.out.println("Le produit selectionné est introuvable ou est indisponible");

                System.out.println("Souhaitez-vous ajouter un autre produit ? 1. Oui, 2.Non");

                try{
                    choice = sc.nextInt();
                }catch (InputMismatchException e)
                {
                    System.out.println("Saisie incorrecte");
                }
            }while (choice == 1);

            //calcul total de la commande optionnel
//            double total = 0;
//
//            for (Produit p: produits)
//            {
//                total += p.getPrix();
//            }

            Commande commande = new Commande();

            //ajout des produits a la commande
            for (Produit p : produits)
            {
                commande.addProduit(p);
                session.refresh(p);
            }

            //LORSQU'IL NE FALLAIT PAS D'ADRESSE
//            boolean create = daoCommande.create(commande);
//
//            if(create)
//            {
//                System.out.println("Commande validée! Encore une petite étape.. ");
//            }
//            else
//            {
//                System.out.println("Erreur lors de commande");
//                haveOrder();
//            }

            //Créer et stocker adresse
            askAdresse(commande);

        }catch (InputMismatchException e)
        {
            System.out.println("Erreur lors de l'entrée des données");
            haveOrder();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void findAllOrders()
    {
        try {
            List<Commande> commandes = daoCommande.findAll();
            if(commandes == null)
                System.out.println("Aucune commande n'a été passé pour le moment");
            else
            {
                //calculer total des commandes
                for (Commande c: commandes) {
                    double total = 0;
                    for (Produit p: c.getProduits()) {
                        total += p.getPrix();
                    }
                    c.setTotal(total);
                    System.out.println(c);
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void findOrdersByDate(){
        try {
            System.out.println("Les commandes de quelle date souhaitez-vous voir? (Format JJ/MM/AAAA)");
            String dateS = sc.next();

            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateS);

            List<Commande> commandes = daoCommande.findOrderByDate(date);
            if(commandes == null)
            {
                System.out.printf("Aucune commande n'a été passé le %s", dateS);
                System.out.println();
            }

            else
            {
                System.out.printf("Voici les commandes passées le %s", dateS);
                System.out.println();
                //calculer total des commandes
                for (Commande c: commandes) {
                    double total = 0;
                    for (Produit p: c.getProduits()) {
                        total += p.getPrix();
                    }
                    c.setTotal(total);
                    System.out.println(c);
                }
            }
        }catch (ParseException e)
        {
            System.out.println("Format de la date non respectée");
            findOrdersByDate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public static void askAdresse(Commande commande)
    {
        try {
            System.out.println("Veuillez saisir l'adresse de livraison svp");
            System.out.println("Numéro et rue :");
            sc.nextLine();
            String rue = sc.nextLine();

            System.out.println("Ville :");
            sc.nextLine();
            String ville = sc.nextLine();

            System.out.println("Code postal");
            String codePostal = sc.next();

            Adresse adresse = new Adresse(rue, ville, codePostal);

            boolean creationAdress = daoAdresse.create(adresse);

            if(creationAdress)
            {
                commande.setAdresse(adresse);
                boolean creationCommande = daoCommande.create(commande);
                if(creationCommande)
                {
                    System.out.println("Adresse enregistrée, commande validée ..");
                }
                else
                {
                    System.out.println("Une erreur est survenue veuillez-recommencer svp");
                    //Commande non enregistrée donc suppression de l'adresse
                    daoAdresse.delete(adresse);
                }

            }
            else
            {
                System.out.println("Une erreur est survenue lors de la création de l'adresse veuillez réessayer ..");
                askAdresse(commande);
            }
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

    }
}


