package org.example.service;

import org.example.dao.IDao;
import org.example.entities.Adresse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

public class AdresseService implements IDao<Adresse> {

    StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    Session session = sessionFactory.openSession();
    @Override
    public boolean create(Adresse o) {
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Adresse o) {
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(Adresse o) {
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public Adresse findById(int id) {
        Adresse adresse = null;
        session.beginTransaction();
        adresse = (Adresse) session.get(Adresse.class, id);
        session.getTransaction().commit();
        return adresse;
    }

    @Override
    public List<Adresse> findAll() {
        List<Adresse> adresses = null;
        adresses = session.createQuery("FROM Adresse").list();
        return adresses;
    }
}
