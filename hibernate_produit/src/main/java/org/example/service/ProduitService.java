package org.example.service;

import org.example.dao.IDao;
import org.example.entities.Produit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.List;

public class ProduitService implements IDao<Produit> {

    static StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    static SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    public static Session session = sessionFactory.openSession();

    @Override
    public boolean create(Produit o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();

            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(Produit o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();

            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Produit o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();

            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Produit findById(int id) {
        Produit produit = null;

        session.beginTransaction();
        produit = (Produit) session.load(Produit.class, id);
        session.getTransaction().commit();

        return produit;
    }

    @Override
    public List<Produit> findAll() {
        try {
            List<Produit> produits = null;

            session.beginTransaction();
            Query<Produit> listQuery = ProduitService.session.createQuery("FROM Produit");
            produits = listQuery.list();
            session.getTransaction().commit();

            return produits;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public List<Produit> findAllProductsPriceUpThanNumber(double number) throws Exception{
        if(number >= 0)
        {
            try {
                List<Produit> produits = null;

                session.beginTransaction();
                Query<Produit> listQuery = ProduitService.session.createQuery("FROM Produit WHERE prix > :nb");
                listQuery.setParameter("nb", number);
                produits = listQuery.list();
                session.getTransaction().commit();

                return produits;
            } catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }throw new Exception("error value");

    }

    public List<Produit> findAllProductsBetweenTwoDates(Date date, Date date2) throws Exception{
       if(date.before(date2))
       {
           try {
               List<Produit> produits = null;

               session.beginTransaction();
               Query<Produit> query = ProduitService.session.createQuery("FROM Produit WHERE dateAchat BETWEEN ?1 AND ?2");
               query.setParameter(1, date);
               query.setParameter(2, date2);
               produits = query.list();
               session.getTransaction().commit();

               return produits;
           } catch (Exception e)
           {
               e.printStackTrace();
               return null;
           }
       }
      throw new Exception("error Date");
    }

    public List findProductsWithStock(int stock){
        try {
            session.beginTransaction();
            Query<Produit> query =  ProduitService.session.createQuery("SELECT id, reference FROM Produit WHERE stock < :stockMax");
            query.setParameter("stockMax", stock);
            List resultList = query.list();
            session.getTransaction().commit();
            return resultList;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public double showValueByBrand(String marque)
    {
        try {
            Query<Double> query =  ProduitService.session.createQuery("SELECT sum(stock*prix) FROM Produit WHERE marque = :marque");
            query.setParameter("marque", marque);
            double result = query.uniqueResult();
            return result;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return -1;
        }
    }

    public double showAvg()
    {
        try {
            Query<Double> query =  ProduitService.session.createQuery("SELECT avg(prix) FROM Produit");
            double result = query.uniqueResult();
            return result;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return -1;
        }
    }

    public List<Produit> showProductsByBrand(String[] marques)
    {
        try {
            Query<Produit> query =  ProduitService.session.createQuery("FROM Produit WHERE marque IN :marques");
            query.setParameterList("marques", marques);
            List<Produit> produits = query.list();
            return produits;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public int deleteProductsByBrand(String marque)
    {
        try {
            Query query =  ProduitService.session.createQuery("DELETE Produit WHERE marque = :marque");
            query.setParameter("marque", marque);
            session.beginTransaction();
            int result = query.executeUpdate();
            session.getTransaction().commit();
            return result;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return 0;
        }
    }

    public List<Produit> getBestProduct(Double note)
    {
        try {
            //REQUETE HQL

            List<Produit> pr = session.createQuery("SELECT p FROM Produit AS p JOIN p.commentaires as c GROUP BY p.id HAVING avg(c.note) >= :note").setParameter("note", note).list();
            return pr;

            //REQUETE SQL
//            List<Produit> produits = findAll();
//            List<Produit> bestProduits = new ArrayList<>();

//            for (Produit p: produits) {
//                if(p.getCommentaires().size()>0)
//                {
//                    int id = p.getId();
//                    double note = (double) session.createSQLQuery("SELECT avg(note) FROM commentaire \n" +
//                            "INNER JOIN produit ON commentaire.produit_id = :id").setParameter("id", id).uniqueResult();
//                    if(note>=4)
//                        bestProduits.add(p);
//                }
//            }
//            return bestProduits;

        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
