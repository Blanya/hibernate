package org.example.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "commande")
public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "com_id")
    private int id;

    @Transient
    private double total;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_achat")
    private Date dateAchat;

    @ManyToMany()
    @JoinTable(name = "produit_commande",
            joinColumns = @JoinColumn(name = "com_id"),
            inverseJoinColumns = @JoinColumn(name = "pr_id"))
    private List<Produit> produits = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "ad_id")
    private Adresse adresse;

    public Commande() {
        this.dateAchat = new Date();
    }

    public Commande(double total, Date dateAchat) {
        this.total = total;
        this.dateAchat = dateAchat;
    }

    public Commande(double total) {
        this.total = total;
        this.dateAchat = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Date getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(Date dateAchat) {
        this.dateAchat = dateAchat;
    }

    public List<Produit> getProduits() {
        return produits;
    }

    public void setProduits(List<Produit> produits) {
        this.produits = produits;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public void addProduit(Produit p)
    {
        produits.add(p);
    }

    @Override
    public String toString() {
        return "Commande{" +
                "id=" + id +
                ", total=" + total +
                ", dateAchat=" + dateAchat +
                ", produits=" + produits +
                ", adresse=" + adresse +
                '}';
    }
}
