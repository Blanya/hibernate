package org.example;

import org.example.entities.Produit;
import org.example.service.ProduitService;
import org.hibernate.query.Query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Test {

    //EX 1
//    public static void main(String[] args) throws ParseException {
//
//        ProduitService daoProduit = new ProduitService();
//
//        //creation de 5 produits
//
//        //Formatage de la date
//        String pattern = "yyyy/MM/dd";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//
//        daoProduit.create(new Produit("Apple", "MB45115", simpleDateFormat.parse("2015/04/25"), 1257.98,9 ));
//        daoProduit.create(new Produit("Apple", "AP4158",  simpleDateFormat.parse("2021/06/25"), 878.81,11 ));
//        daoProduit.create(new Produit("Samsung", "S22", simpleDateFormat.parse("2021/10/29"), 997.51,5 ));
//        daoProduit.create(new Produit("Samsung", "S48528", simpleDateFormat.parse("2022/04/01"), 15.52,3 ));
//        daoProduit.create(new Produit("Samsung", "S6586", simpleDateFormat.parse("2022/04/25"), 1287.9,9 ));
//
//        //afficher produit dont l'ID est 2
//
//        Produit produit = daoProduit.findById(2);
//        System.out.println(produit);
//
//        //supprimer le produit dont l'id est 3
//
//        boolean answer = daoProduit.delete(daoProduit.findById(3));
//        if(answer)
//            System.out.println("Le produit avec l'id 3 a été supprimé");
//
//        //modifier produit avec l'id 1
//
//        Produit produit1 = daoProduit.findById(1);
//        if(produit1 != null)
//        {
//            produit1.setMarque("Asus");
//            daoProduit.update(produit1);
//        }
//
//        ProduitService.session.close();
//    }


//    EX2

//    public static void main(String[] args) throws Exception {
//        ProduitService daoProduit = new ProduitService();
//
//        //Formatage de la date
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
//
////        daoProduit.create(new Produit("Apple", "MB45115", simpleDateFormat.parse("2015/04/25"), 57.98,9 ));
//
//        //Afficher tous les produits
//
//        List<Produit> produits = daoProduit.findAll();
//        produits.forEach(System.out::println);
//
//        //Afficher les produits dont le prix est supérieur a 100e
//
//        List<Produit> produitsList = daoProduit.findAllProductsPriceUpThanNumber(100);
//        produitsList.forEach(System.out::println);
//
//        //Afficher produits entre 2 dates
//
//        Date date = simpleDateFormat.parse("2021/04/25");
//        Date date2 = simpleDateFormat.parse("2022/04/02");
//        List<Produit> products = daoProduit.findAllProductsBetweenTwoDates(date, date2);
//        products.forEach(System.out::println);
//
//        ProduitService.session.close();
//    }

    public static void main(String[] args){
//        askDates();
        askStock();
    }

    public static void askDates(){
        ProduitService daoProduit = new ProduitService();
        Scanner sc = new Scanner(System.in);

        try{
            System.out.println("Donnez-moi une date au format JJ/MM/AAAA");
            String dateS = sc.next();
            System.out.println("Donnez-moi une autre date au format JJ/MM/AAAA, plus récente que la première");
            String date2S = sc.next();

            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateS);
            Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(date2S);

            List<Produit> products = daoProduit.findAllProductsBetweenTwoDates(date, date2);
            System.out.printf("Voici les produits compris entre le %s et le %s", dateS, date2S);
            products.forEach(System.out::println);

        }catch (ParseException e) {
            System.out.println("Erreur de format de dates");
            askDates();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        ProduitService.session.close();
    }

    public static void askStock(){
        ProduitService daoProduit = new ProduitService();
        Scanner sc = new Scanner(System.in);

        try{
            System.out.println("Donnez-moi une limite de stock");
            int stock = sc.nextInt();

            Query<Produit> query =  ProduitService.session.createQuery("SELECT id, reference FROM Produit WHERE stock < :stockMax");
            query.setParameter("stockMax", stock);
            List resultList = query.list();

            List<Produit> produits = new ArrayList<>();

//            for (Object o: resultList) {
//                Object[] res = (Object[]) o;
//                Produit pr = new Produit();
//                pr.setId((int) res[0]);
//                pr.setReference((String) res[1]);
//
//                produits.add(pr);
//            }
//
//            System.out.printf("Voici les produits dont le stock est inférieur à %d", stock);
//            produits.forEach(System.out::println);

            System.out.printf("Voici les produits(ref + id) dont le stock est inférieur à %d", stock);
            System.out.println();

            for (Object o : resultList) {
                Object[] res = (Object[]) o;
                System.out.println("Id: " + res[0] + ", Reference: " + res[1]);
            }

        }catch (InputMismatchException e) {
            System.out.println("Erreur de format, veuillez entrer un nombre entier");
            askStock();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        ProduitService.session.close();
    }

}

